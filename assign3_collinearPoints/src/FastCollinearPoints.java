import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Collections;

/**
 * Created by lorenzo on 28.09.2015.
 */
public class FastCollinearPoints {
  private static final int MIN_REQUIRED_COLLINEAR_POINTS = 3;
  private Point[] points;
  private ArrayList<Point> pts;
  private ArrayList<LineSegment> segments = new ArrayList<>();
  private HashSet<Segment> segmentsSet = new HashSet<>();

  public FastCollinearPoints(Point[] points) {
    if (points == null) throw new NullPointerException();

    for (Point p : points) {
      if (p == null) throw new NullPointerException();

      for (Point p1 : points) {
        if (p != p1 && p.compareTo(p1) == 0) throw new IllegalArgumentException();
      }
    }

    this.points = Arrays.copyOf(points, points.length);
    pts = new ArrayList<>(Arrays.asList(points));

    computeLineSegments();
  }

  public int numberOfSegments() {
    return segments.size();
  }

  public LineSegment[] segments() {
    return segments.toArray(new LineSegment[segments.size()]);
  }

  private void computeLineSegments() {
    for (Point currentPoint : points) {                               // O(N)
      Comparator<Point> pointComparator = currentPoint.slopeOrder();
      Collections.sort(pts, pointComparator);                         // O(NLogN) - hopefully
      addSegmentsToSegmentSet(currentPoint, pointComparator);         // O(N)
    }

    for (Segment s: segmentsSet) segments.add(s.ls);
  }

  // inspired from 3 way partitioning
  private void addSegmentsToSegmentSet(Point currentPoint, Comparator<Point> pointComparator) {
    ArrayList<Point> currentList = new ArrayList<>();

    int i, j;
    for (i = 0, j = 1; j < pts.size(); j++) {
      if (pointComparator.compare(pts.get(i), pts.get(j)) == 0) {
        currentList.add(pts.get(j));
      } else {
        checkCollinearPoints(currentPoint, i, j, currentList);

        i = j;
        currentList = new ArrayList<>();
      }
    }

    checkCollinearPoints(currentPoint, i, j, currentList); // one extra check for last point -? a do while might do the job
  }

  private void checkCollinearPoints(Point pt, int i, int j, ArrayList<Point> ptList) {
    if ((j - i) >= MIN_REQUIRED_COLLINEAR_POINTS) { // 3 collinear points with respect to current point
      ptList.add(pts.get(i));   // first collinear point should be added to list
      ptList.add(pt);           // reference point should be added to list
      segmentsSet.add(new Segment(Collections.min(ptList), Collections.max(ptList)));
    }
  }

  private class Segment {
    private Point startPt;
    private Point endPt;
    private LineSegment ls;

    public Segment(Point p1, Point p2) {
      this.startPt = p1;
      this.endPt = p2;
      ls = new LineSegment(p1, p2);
    }

    @Override
    public boolean equals(Object that) {
      if (this == that) return true;
      if (that == null || getClass() != that.getClass()) return false;

      Segment segment = (Segment) that;

      if (this.startPt == null || segment.startPt == null
              || this.endPt == null || segment.endPt == null) return false;

      return (startPt.compareTo(segment.startPt) == 0) && (endPt.compareTo(segment.endPt) == 0);
    }

    @Override
    public int hashCode() {
      return ls.toString().hashCode();
    }
  }
}
