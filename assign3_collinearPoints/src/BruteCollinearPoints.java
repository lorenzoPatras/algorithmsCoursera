import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by lorenzo on 28.09.2015.
 */
public class BruteCollinearPoints {
  private final Point[] points;
  private ArrayList<LineSegment> segments = new ArrayList<>();

  public BruteCollinearPoints(Point[] points) {
    if (points == null) throw new NullPointerException();

    for (Point p : points) {
      if (p == null) throw new NullPointerException();

      for (Point p1 : points) {
        if (p != p1 && p.compareTo(p1) == 0) throw new IllegalArgumentException();
      }
    }

    this.points = Arrays.copyOf(points, points.length);
    Arrays.sort(this.points);

    computeLineSegments();
  }

  public int numberOfSegments() {
    return segments.size();
  }

  public LineSegment[] segments() {
    return segments.toArray(new LineSegment[segments.size()]);
  }

  private void computeLineSegments() {
    for (int p = 0; p < points.length - 3; p++) {
      for (int q = p + 1; q < points.length - 2; q++) {
        for (int r = q + 1; r < points.length - 1; r++) {
          for (int s = r + 1; s < points.length; s++) {
            double s1 = points[p].slopeTo(points[q]);
            double s2 = points[q].slopeTo(points[r]);
            double s3 = points[r].slopeTo(points[s]);
            double s4 = points[p].slopeTo(points[s]);

            if (s1 == s2 && s3 == s4) {
              segments.add(new LineSegment(points[p], points[s]));
            }
          }
        }
      }
    }
  }
}
