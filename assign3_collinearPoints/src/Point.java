import edu.princeton.cs.algs4.StdDraw;

import java.util.Comparator;

/**
 * Created by lorenzo on 28.09.2015.
 */
public class Point implements Comparable<Point> {

  private final int x;
  private final int y;

  public Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public void draw() {
    StdDraw.point(x, y);
  }

  public void drawTo(Point that) {
    StdDraw.line(this.x, this.y, that.x, that.y);
  }

  //Returns the slope between this point and the specified point.
  // slope = (y1 - y0) / (x1 - x0);
  // if slope is horizontal then it will be 0;
  // if slope is vertical it will be Double.POSITIVE_INFINITY;
  // if (x0, y0) == (x1, y1) then the slope will be Double.NEGATIVE_INFINITY
  public double slopeTo(Point that) {
    if (this.x == that.x && this.y == that.y) return Double.NEGATIVE_INFINITY;
    if (this.y == that.y) return 0;
    if (this.x == that.x) return Double.POSITIVE_INFINITY;
    return ((double) that.y - (double) this.y) / ((double) that.x - (double) this.x);
  }

  @Override
  public int compareTo(Point point) {
    if (this.y < point.y) return -1;
    else if (this.y > point.y) return 1;
    else if (this.x < point.x) return -1;
    else if (this.x > point.x) return 1;
    return 0;
  }

  // Compares two points by the slope they make with this point.
  public Comparator<Point> slopeOrder() {
    return new Comparator<Point>() {
      @Override
      public int compare(Point pt1, Point pt2) {
        if (slopeTo(pt1) < slopeTo(pt2)) return -1;
        if (slopeTo(pt1) > slopeTo(pt2)) return 1;
        return 0;
      }
    };
  }

  public String toString() {
    return "(" + x + ", " + y + ")";
  }
}
