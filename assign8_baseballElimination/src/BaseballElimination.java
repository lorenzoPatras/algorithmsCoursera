import edu.princeton.cs.algs4.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by lor on 30.11.2015.
 */
public class BaseballElimination {
  private int teamsNr;
  private Games games;
  private HashMap<String, Team> teams           = new HashMap<>();
  private HashMap<String, Integer> teamIndexMap = new HashMap<>();
  private HashMap<Integer, String> indexTeamMap = new HashMap<>();

  public BaseballElimination(String filename) {
    In file = new In(filename);

    teamsNr = Integer.parseInt(file.readLine());
    games   = new Games(teamsNr);

    for (int i = 0; i < teamsNr; i++) {
      processLine(file.readLine(), i);
    }
  }

  private void processLine(String line, int index) {
    line            = line.trim();                     // get rid of leading or trailing spaces
    String[] elems  = line.split("\\s+");
    String teamName = elems[0];
    int wins        = Integer.parseInt(elems[1]);
    int losses      = Integer.parseInt(elems[2]);
    int remaining   = Integer.parseInt(elems[3]);

    teams.put(teamName, new Team(teamName, wins, losses, remaining));
    teamIndexMap.put(teamName, index);
    indexTeamMap.put(index, teamName);

    for (int i = 0; i < teamsNr; i++) { // construct the games grid
      games.addGames(index, i, Integer.parseInt(elems[4 + i]));
    }
  }

  public int numberOfTeams() {
    return teamsNr;
  }

  public Iterable<String> teams() { return teams.keySet(); }

  public int wins(String team) {
    checkTeamExistence(team);
    return teams.get(team).wins;
  }

  public int losses(String team) {
    checkTeamExistence(team);
    return teams.get(team).losses;
  }

  public int remaining(String team) {
    checkTeamExistence(team);
    return teams.get(team).remaining;
  }

  public int against(String team1, String team2) {
    checkTeamExistence(team1);
    checkTeamExistence(team2);
    return games.getRemainingGames(teamIndexMap.get(team1), teamIndexMap.get(team2));
  }

  public boolean isEliminated(String team) {
    checkTeamExistence(team);

    return isTriviallyEliminated(team) || isNonTriviallyEliminated(team);
  }

  private boolean isTriviallyEliminated(String team) {
    int maxPossibleWins = teams.get(team).wins + teams.get(team).remaining;

    for (Team t : teams.values()) { // search for a team which already has more wins than the maximum possible
      if (maxPossibleWins < t.wins) return true;
    }

    return false;
  }

  private boolean isNonTriviallyEliminated(String team) {
    FlowNetwork networkFlow = constructNetworkFlow(team);
    int sink = choose(teamsNr - 1, 2) + teamsNr;
    FordFulkerson ff = new FordFulkerson(networkFlow, 0, sink);

    for (FlowEdge f : networkFlow.adj(0)) { // if all the edges from source are saturated the team is not eliminated
      if (f.capacity() != f.flow()) return true;
    }

    return false;
  }

  // compute the combinations (of matches between all the teams)
  private int choose(int total, int choose){
    if(total < choose)
      return 0;
    if(choose == 0 || choose == total)
      return 1;
    return choose(total-1,choose-1)+choose(total-1,choose);
  }

  private FlowNetwork constructNetworkFlow(String team) {
    int queryTeamIndex = teamIndexMap.get(team);
    int matchesCombinations = choose(teamsNr - 1, 2);

    FlowNetwork networkFlow = new FlowNetwork(teamsNr + 1 + matchesCombinations);

    // construct the edges from s to match combinations and from match combinations to winners
    // process only elements above the main diagonal
    // don't take into consideration the query team
    int vertexNr = 1; // start from 1 because 0 is assigned to s

    for (int i = 0; i < teamsNr - 1; i++) {
      if (i == queryTeamIndex) continue;

      for (int j = i + 1; j < teamsNr; j++) {
        if (j == queryTeamIndex) continue;

        FlowEdge gamePayload  = new FlowEdge(0, teamsNr + vertexNr, games.getRemainingGames(i, j));
        FlowEdge possibleWin1 = new FlowEdge(teamsNr + vertexNr, i + 1, Double.POSITIVE_INFINITY);
        FlowEdge possibleWin2 = new FlowEdge(teamsNr + vertexNr, j + 1, Double.POSITIVE_INFINITY);

        networkFlow.addEdge(gamePayload);
        networkFlow.addEdge(possibleWin1);
        networkFlow.addEdge(possibleWin2);

        vertexNr++;
      }
    }

    // connect team vertices to t
    for (int i = 0; i < teamsNr; i++) {
      if (i == queryTeamIndex) continue;

      int capacity  = teams.get(team).wins + teams.get(team).remaining - teams.get(indexTeamMap.get(i)).wins;
      FlowEdge sink = new FlowEdge(i + 1, matchesCombinations + teamsNr, capacity);

      networkFlow.addEdge(sink);
    }

    return networkFlow;
  }

  public Iterable<String> certificateOfElimination(String team) {
    checkTeamExistence(team);


    if (isTriviallyEliminated(team)) {
      ArrayList<String> teamSubset = new ArrayList<>();
      teamSubset.add(getTeamForTrivialElimination(team));

      return teamSubset;
    }

    if (isNonTriviallyEliminated(team)) return getTeamsForNonTrivialElimination(team);

    return null;
  }

  private String getTeamForTrivialElimination(String team) {
    int maxPossibleWins = teams.get(team).wins + teams.get(team).remaining;

    for (Team t : teams.values()) {
      if (maxPossibleWins < t.wins) return t.name;
    }

    return null;
  }

  private Iterable<String> getTeamsForNonTrivialElimination(String team) {
    FlowNetwork networkFlow = constructNetworkFlow(team);
    FordFulkerson ff = new FordFulkerson(networkFlow, 0, choose(teamsNr - 1, 2) + teamsNr);

    ArrayList<String> ret = new ArrayList<>();

    int teamIndex = teamIndexMap.get(team);
    for (int i = 0; i < teamsNr; i++) {
      if (i == teamIndex) continue;

      if (ff.inCut(i + 1)) ret.add(indexTeamMap.get(i));
    }

    return ret;
  }

  private void checkTeamExistence(String team) {
    if (!teamIndexMap.keySet().contains(team)) throw new IllegalArgumentException("team does not exist");
  }


  private class Team {
    private String name;
    private int wins;
    private int losses;
    private int remaining;

    public Team(String name, int wins, int losses, int remaining) {
      this.name = name;
      this. wins = wins;
      this.losses = losses;
      this.remaining = remaining;
    }
  }

  private class Games {
    private int[][] games;

    public Games (int teamsNr) {
      games = new int[teamsNr][teamsNr];
    }

    public void addGames(int teamI, int teamJ, int toPlay) {
      games[teamI][teamJ] = toPlay;
    }

    public int getRemainingGames(int teamI, int teamJ) {
      return games[teamI][teamJ];
    }
  }


  public static void main(String[] args) {
    BaseballElimination division = new BaseballElimination("teams5.txt");
    for (String team : division.teams()) {
      if (division.isEliminated(team)) {
        StdOut.print(team + " is eliminated by the subset R = { ");
        for (String t : division.certificateOfElimination(team)) {
          StdOut.print(t + " ");
        }
        StdOut.println("}");
      }
      else {
        StdOut.println(team + " is not eliminated");
      }
    }
  }

}
