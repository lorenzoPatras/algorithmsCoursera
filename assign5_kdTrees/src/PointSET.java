import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by lorenzo on 05.10.2015.
 */
public class PointSET {
  private TreeSet<Point2D> points;


  public PointSET() {
    points = new TreeSet<>();
  }


  public boolean isEmpty() {
    return points.isEmpty();
  }

  public int size() {
    return points.size();
  }

  public void insert(Point2D p) {
    if (p == null) throw new NullPointerException();

    points.add(p);
  }

  public boolean contains(Point2D p) {
    if (p == null) throw new NullPointerException();
    return points.contains(p);
  }

  public void draw() {
    for (Point2D p : points) {
      StdDraw.point(p.x(), p.y());
    }
  }

  public Iterable<Point2D> range(RectHV rect) {
    if (rect == null) throw new NullPointerException();

    ArrayList<Point2D> rangePoints = new ArrayList<>();

    for (Point2D p : points) {
      if (p.x() <= rect.xmax() && p.x() >= rect.xmin() && p.y() <= rect.ymax() && p.y() >= rect.ymin())
        rangePoints.add(p);
    }

    return rangePoints;
  }

  public Point2D nearest(Point2D p) {
    if (p == null) throw new NullPointerException();
    if (isEmpty()) return null;

    double minDistance = Double.MAX_VALUE;
    Point2D ret = null;

    for (Point2D pt : points) {
      double distance = Math.sqrt(Math.pow((p.x() - pt.x()), 2) + Math.pow((p.y() - pt.y()), 2));

      if (distance < minDistance) {
        minDistance = distance;
        ret = pt;
      }
    }

    return ret;
  }
}
