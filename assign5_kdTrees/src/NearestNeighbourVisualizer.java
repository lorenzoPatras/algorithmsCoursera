import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.StdDraw;
import edu.princeton.cs.algs4.StdOut;

/**
 * Created by lorenzo on 05.10.2015.
 */
public class NearestNeighbourVisualizer {
  public static void main(String[] args) {
    String filename = args[0];
    In in = new In(filename);

    StdDraw.show(0);

    // initialize the two data structures with point from standard input
    PointSET brute = new PointSET();
    KdTree kdtree = new KdTree();
    while (!in.isEmpty()) {
      double x = in.readDouble();
      double y = in.readDouble();
      Point2D p = new Point2D(x, y);
      kdtree.insert(p);
      brute.insert(p);
    }

    Point2D bruteN = new Point2D(0, 0);
    Point2D kdN = new Point2D(0, 0);
    while (true) {

      if (StdDraw.mousePressed()) {
        double x = StdDraw.mouseX();
        double y = StdDraw.mouseY();
        StdOut.printf("mouse %8.6f %8.6f\n", x, y);
        StdOut.printf("brute %8.6f %8.6f\n", bruteN.x(), bruteN.y());
        StdOut.printf("kd %8.6f %8.6f\n", kdN.x(), kdN.y());
      }

      // the location (x, y) of the mouse
      double x = StdDraw.mouseX();
      double y = StdDraw.mouseY();
      Point2D query = new Point2D(x, y);

      // draw all of the points
      StdDraw.clear();
      StdDraw.setPenColor(StdDraw.BLACK);
      StdDraw.setPenRadius(.01);
      brute.draw();

      // draw in red the nearest neighbor (using brute-force algorithm)
      StdDraw.setPenRadius(.03);
      StdDraw.setPenColor(StdDraw.RED);
      bruteN = brute.nearest(query); bruteN.draw();
      StdDraw.setPenRadius(.02);

      // draw in blue the nearest neighbor (using kd-tree algorithm)
      StdDraw.setPenColor(StdDraw.BLUE);
      kdN = kdtree.nearest(query); kdN.draw();
      StdDraw.show(0);
      StdDraw.show(40);
    }
  }
}
