import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.util.ArrayList;

/**
 * Created by lorenzo on 05.10.2015.
 */
public class KdTree {
  private Node root;

  public KdTree() {
    root = null;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public int size() {
    return size(root);
  }

  private int size(Node node) {
    if (node == null) return 0;

    return node.rank;
  }

  public void insert(Point2D p) {
    if (p == null) throw new NullPointerException();

    root = insert(root, p, -1);
  }

  private Node insert(Node node, Point2D p, int level) {
    if (node == null) return new Node(1, level + 1, p);

    if (node.point.equals(p)) return node;

    if (node.level % 2 == 0) { // sort by x on even levels
      if (p.x() < node.point.x()) node.left = insert(node.left, p, node.level);
      else node.right = insert(node.right, p, node.level);
    } else { // sort by y on odd levels
      if (p.y() < node.point.y()) node.left = insert(node.left, p, node.level);
      else node.right = insert(node.right, p, node.level);
    }

    node.rank = 1 + size(node.left) + size(node.right);
    return node;
  }

  public boolean contains(Point2D p) {
    if (p == null) throw new NullPointerException();

    return (getNode(root, p) != null);
  }

  private Node getNode(Node node, Point2D p) {
    if (node == null) return null;

    if (p.equals(node.point)) return node;

    if (node.level % 2 == 0) { // sort by x on even levels
      if (p.x() < node.point.x()) return getNode(node.left, p);
      else return getNode(node.right, p);
    } else { // sort by y on odd levels
      if (p.y() < node.point.y()) return getNode(node.left, p);
      else return getNode(node.right, p);
    }
  }

  public void draw() {
    draw(root, new Point2D(1, 1), new Point2D(0, 0));
  }

  // need a review: it always draws from the margin -> rethink the bounds
  private void draw(Node node, Point2D upperBound, Point2D lowerBound) {
    if (node == null) return;


    if (node.level % 2 == 0) {
      StdDraw.setPenColor(StdDraw.RED);
      StdDraw.setPenRadius(.001);
      StdDraw.line(node.point.x(), lowerBound.y(), node.point.x(), upperBound.y());
    } else {
      StdDraw.setPenColor(StdDraw.BLUE);
      StdDraw.setPenRadius(.001);
      StdDraw.line(lowerBound.x(), node.point.y(), upperBound.x(), node.point.y());
    }

    draw(node.left, new Point2D(0, 0), node.point);
    draw(node.right, node.point, new Point2D(1, 1));

  }

  public Iterable<Point2D> range(RectHV rect) {
    if (rect == null) throw new NullPointerException();

    ArrayList<Point2D> ret = new ArrayList<>();
    getPointsInRange(root, rect, ret);

    return ret;
  }

  // there is a problem somewhere -> see 10 points and you will understand
  private void getPointsInRange(Node node, RectHV rect, ArrayList<Point2D> container) {
    if (node == null) return;

    // check if node lies inside the rectangle
    if (rect.contains(node.point)) container.add(node.point);

    if (node.level % 2 == 0) { // check rectangle relative position to the point
      if (rect.xmax() < node.point.x()) getPointsInRange(node.left, rect, container);
      else if (rect.xmin() > node.point.x()) getPointsInRange(node.right, rect, container);
      else {
        getPointsInRange(node.left, rect, container);
        getPointsInRange(node.right, rect, container);
      }
    } else {
      if (rect.ymax() < node.point.y()) getPointsInRange(node.left, rect, container);
      else if (rect.ymin() > node.point.y()) getPointsInRange(node.right, rect, container);
      else {
        getPointsInRange(node.left, rect, container);
        getPointsInRange(node.right, rect, container);
      }
    }
  }

  public Point2D nearest(Point2D p) {
    if (p == null) throw new NullPointerException();

    Point2D nearest = getNearest(root, p, null, Double.MAX_VALUE);

    return nearest;
  }

  // fucked up!!!
  private Point2D getNearest(Node node, Point2D queryPt, Point2D nearest, double bestDistance) {
    if (node == null) return nearest;

    double currentDistance = queryPt.distanceTo(node.point);
    if (currentDistance < bestDistance) {
      bestDistance = currentDistance;
      nearest = new Point2D(node.point.x(), node.point.y());
    }

    if (node.level % 2 == 0) { // check rectangle relative position to the point
      if (queryPt.x() < node.point.x()) nearest = getNearest(node.left, queryPt, nearest, bestDistance);
      else nearest = getNearest(node.right, queryPt, nearest, bestDistance);
    } else {
      if (queryPt.y() < node.point.y()) nearest = getNearest(node.left, queryPt, nearest, bestDistance);
      else nearest = getNearest(node.right, queryPt, nearest, bestDistance);
    }

    bestDistance = nearest.distanceTo(queryPt);
    double lineDistance = distanceToLine(queryPt, node.point, node.level);
    if (lineDistance < bestDistance) {
      if (node.level % 2 == 0) { // check rectangle relative position to the point
        if (queryPt.x() < node.point.x()) nearest = getNearest(node.right, queryPt, nearest, bestDistance);
        else nearest = getNearest(node.left, queryPt, nearest, bestDistance);
      } else {
        if (queryPt.y() < node.point.y()) nearest = getNearest(node.right, queryPt, nearest, bestDistance);
        else nearest = getNearest(node.left, queryPt, nearest, bestDistance);
      }
    }

    return nearest;
  }

  private double distanceToLine(Point2D queryPt, Point2D pt, int level) {
    if (level % 2 == 0) {
      return Math.sqrt((queryPt.x() - pt.x()) * (queryPt.x() - pt.x())); // y will be the same => 0
    } else {
      return Math.sqrt((queryPt.y() - pt.y()) * (queryPt.y() - pt.y()));
    }
  }


  private class Node {
    private int rank;
    private int level;
    private Point2D point;
    private Node left, right;

    public Node(int rank, int level, Point2D point) {
      this.rank = rank;
      this.level = level;
      this.point = point;
    }
  }
}
