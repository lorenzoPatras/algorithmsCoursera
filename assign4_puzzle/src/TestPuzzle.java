/**
 * Created by lorenzo on 15.10.2015.
 */
public class TestPuzzle {
  public static void main(String[] args) {
    int[][] bord = {{8, 2, 3},
            {7, 1, 0},
            {5, 4, 6}};

    Board board = new Board(bord);
    Solver babap = new Solver(board);

    System.out.println(babap.moves());
    System.out.println(babap.isSolvable());
    for (Board p : babap.solution()) {
      System.out.println("manh " + p.manhattan());
      System.out.println(p);
    }
  }
}
