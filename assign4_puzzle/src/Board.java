import edu.princeton.cs.algs4.StdRandom;

import java.util.ArrayList;

/**
 * Created by lorenzo on 30.09.2015.
 */
public class Board {
  private final int[][] blocks;
  private int manhattanDistance;
  private int dimension;


  public Board(int[][] blocks) {
    this.blocks = new int[blocks.length][];
    for (int i = 0; i < blocks.length; i++) {
      this.blocks[i] = new int[blocks.length];
      for (int j = 0; j < blocks[i].length; j++) this.blocks[i][j] = blocks[i][j];
    }

    dimension = blocks.length;
    manhattanDistance = -1;
  }

  // returns board dimension
  public int dimension() {
    return dimension;
  }

  // returns Hamming encoding of the board (how many tiles are out of place)
  public int hamming() {
    int hammingDistance = 0;

    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        hammingDistance += (blocks[i][j] ==  i * dimension + j + 1) ? 0 : 1;
      }
    }

    hammingDistance--; // we should not take into consideration the position of 0 when computing the hamming distance

    return hammingDistance;
  }

  // return Manhattan encoding of the board (Manhattan distance between actual position of a tile and its final position)
  public int manhattan() {
    manhattanDistance = 0;

    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        if (blocks[i][j] != 0) {
          int goalI = (blocks[i][j] - 1) / dimension;
          int goalJ = (blocks[i][j] - 1) % dimension;
          int abs = (Math.abs(i - goalI) + Math.abs(j - goalJ));
          manhattanDistance += abs;
        }
      }
    }
    return manhattanDistance;
  }

  // is the board solved
  public boolean isGoal() {
    int[][] goal = new int[dimension][dimension];
    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        goal[i][j] = i * dimension + j + 1;
      }
    }
    goal[dimension - 1][dimension - 1] = 0;

    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        if (goal[i][j] != blocks[i][j]) return false;
      }
    }

    return true;
  }

  // improve make twin by swaping two cells on the first line
  // a board that is obrained by changing any pair of blocks
  public Board twin() {
    int i1, i2, j1, j2;

    int[][] newBlocks = new int[dimension][dimension];
    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        newBlocks[i][j] = blocks[i][j];
      }
    }

    i1 = StdRandom.uniform(dimension);
    j1 = StdRandom.uniform(dimension);
    i2 = StdRandom.uniform(dimension);
    j2 = StdRandom.uniform(dimension);

    while (newBlocks[i1][j1] == 0 || newBlocks[i2][j2] == 0 || newBlocks[i1][j1] == newBlocks[i2][j2]) {
      i1 = StdRandom.uniform(dimension);
      j1 = StdRandom.uniform(dimension);
      i2 = StdRandom.uniform(dimension);
      j2 = StdRandom.uniform(dimension);
    }

    int holder = newBlocks[i1][j1];
    newBlocks[i1][j1] = newBlocks[i2][j2];
    newBlocks[i2][j2] = holder;

    return new Board(newBlocks);
  }

  // get all neighbouring boards
  public Iterable<Board> neighbors() {
    ArrayList<Board> ret = new ArrayList<>();

    // improve -> make 1 for instead of two
    // find 0
    int i0 = 0, j0 = 0;
    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        if (blocks[i][j] == 0) {
          i0 = i;
          j0 = j;
        }
      }
    }

    // create 4 copies of the blocks
    int[][][] blocksCopy = new int[4][dimension][dimension];

    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        for (int k = 0; k < 4; k++) {
          blocksCopy[k][i][j] = blocks[i][j];
        }
      }
    }

    int temp = 0;
    if (i0 != 0) { // you can move upwards (- manhattan)
      temp = blocksCopy[0][i0][j0];
      blocksCopy[0][i0][j0] = blocksCopy[0][i0-1][j0];
      blocksCopy[0][i0-1][j0] = temp;
      ret.add(new Board(blocksCopy[0]));
    }

    if (j0 != 0) { // you can move left (-1 manhattan)
      temp = blocksCopy[2][i0][j0];
      blocksCopy[2][i0][j0] = blocksCopy[2][i0][j0 - 1];
      blocksCopy[2][i0][j0 - 1] = temp;
      ret.add(new Board(blocksCopy[2]));
    }

    if (i0 != dimension - 1) { // you can move downwards (+1 manhattan)
      temp = blocksCopy[1][i0][j0];
      blocksCopy[1][i0][j0] = blocksCopy[1][i0 + 1][j0];
      blocksCopy[1][i0 + 1][j0] = temp;
      ret.add(new Board(blocksCopy[1]));
    }



    if (j0 != dimension - 1) { // you can move upwards
      temp = blocksCopy[3][i0][j0];
      blocksCopy[3][i0][j0] = blocksCopy[3][i0][j0 + 1];
      blocksCopy[3][i0][j0 + 1] = temp;
      ret.add(new Board(blocksCopy[3]));
    }

    return ret;
  }

  @Override
  public boolean equals(Object that) {
    if (that == null) return false;
    if (!(that instanceof Board)) return false;
    if (dimension != ((Board) that).dimension) return false;

    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        if (blocks[i][j] != ((Board) that).blocks[i][j]) return false;
      }
    }

    return true;
  }

  public String toString() {
    StringBuilder ret = new StringBuilder();
    ret.append(dimension + "\n");

    for (int i = 0; i < dimension; i++) {
      for (int j = 0; j < dimension; j++) {
        ret.append(String.format("%2d ", blocks[i][j]));
      }
      ret.append("\n");
    }

    return ret.toString();
  }
}
