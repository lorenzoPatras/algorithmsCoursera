import edu.princeton.cs.algs4.MinPQ;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by lorenzo on 30.09.2015.
 */
public class Solver {
  private boolean isSolvable = false;
  private int nrOfMoves = 0;
  private final Board initial;
  private ArrayList<Board> solution;
  private MinPQ<SearchNode> initialQueue = new MinPQ<>();
  private MinPQ<SearchNode> twinQueue = new MinPQ<>();

  // use A* algorithm to solve the board
  public Solver(Board initial) {
    if (initial == null) throw new NullPointerException();
    this.initial = initial;

    solvePuzzle();
  }

  public boolean isSolvable() {
    return isSolvable;
  }

  // returns the minimum nr of moves needed to solve the initial board (-1 if unsolvable)
  public int moves() {
    return nrOfMoves;
  }

  // returns the sequence of boards in the shortest solution (null if unsolvable)
  public Iterable<Board> solution() {
    return solution;
  }

  private void solvePuzzle() {
    Board twin = initial.twin();

    SearchNode minInitial =  new SearchNode(initial, null, 0, initial.manhattan());
    SearchNode minTwin = new SearchNode(twin, null, 0, twin.manhattan());

    while (!minInitial.board.isGoal() && !minTwin.board.isGoal()) {
      if (minInitial.compareTo(minTwin) < 0) {
        minInitial = continueOnNode(minInitial, initialQueue);
      } else {
        minTwin = continueOnNode(minTwin, twinQueue);
      }
    }

    if (minTwin.board.isGoal()) {
      setSolvableState(minTwin, false);
      return;
    }

    setSolvableState(minInitial, true);
    buildSequenceOfSolvingMoves(minInitial);
  }

  private void setSolvableState(SearchNode node, boolean state) {
    isSolvable = state;
    nrOfMoves = state ? node.movesNr : -1;
  }

  private SearchNode continueOnNode(SearchNode node, MinPQ<SearchNode> que) {
    Iterable<Board> initialNeighbours = node.board.neighbors();
    for (Board b : initialNeighbours) {
      if (!wasChecked(b, node))
        que.insert(new SearchNode(b, node, node.movesNr + 1, b.manhattan()));
    }

    node = que.delMin();
    return node;
  }

  private boolean wasChecked(Board board, SearchNode currentNode) {
    while (currentNode.prevNode != null) {
      if (board.equals(currentNode.prevNode.board)) return true;
      currentNode = currentNode.prevNode;
    }

    return false;
  }

  private void buildSequenceOfSolvingMoves(SearchNode minInitial) {
    solution = new ArrayList<>();

    while (minInitial != null) {
      solution.add(minInitial.board);
      minInitial = minInitial.prevNode;
    }

    Collections.reverse(solution);
  }


  private class SearchNode implements Comparable<SearchNode> {
    private Board board;
    private SearchNode prevNode;
    private int movesNr;
    private int distance;

    public SearchNode(Board board, SearchNode prevNode, int movesNr, int distance) {
      this.board = board;
      this.prevNode = prevNode;
      this.movesNr = movesNr;
      this.distance = distance;
    }

    @Override
    public int compareTo(SearchNode searchNode) {
      if ((this.movesNr + this.distance) < (searchNode.movesNr + searchNode.distance)) return -1;
      if ((this.movesNr + this.distance) > (searchNode.movesNr + searchNode.distance)) return 1;
      if (this.distance < searchNode.distance) return -1;
      if (this.distance > searchNode.distance) return 1;
      return 0;
    }
  }

}
