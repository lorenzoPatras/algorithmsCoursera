# if [[$1 == "testEncode"]];
#   then
# fi
if [[ $1 == testEncode ]]; then
  echo "test the encoding"
  #     # intelliJ will output the build result in /out/production/BurrowsWheeler
  java -cp ./out/production/BurrowsWheeler/:../lib/algs4.jar MoveToFront - < testData/burrows/abra.txt | java -cp ../lib/algs4.jar edu.princeton.cs.algs4.HexDump 16
fi

if [[ $1 == testDecode ]]; then
  echo "test the decoding"
  #     # intelliJ will output the build result in /out/production/BurrowsWheeler
  java -cp ./out/production/BurrowsWheeler/:../lib/algs4.jar MoveToFront - < testData/burrows/abra.txt | java -cp ./out/production/BurrowsWheeler/:../lib/algs4.jar MoveToFront +
fi

if [[ $1 == testEncodeBW ]]; then
  echo "test the encoding Burrows-Wheeler"
  #     # intelliJ will output the build result in /out/production/BurrowsWheeler
  java -cp ./out/production/BurrowsWheeler/:../lib/algs4.jar BurrowsWheeler - < testData/burrows/abra.txt | java -cp ../lib/algs4.jar edu.princeton.cs.algs4.HexDump 16
fi

if [[ $1 == testDecodeBW ]]; then
  echo "test the decoding Burrows-Wheeler"
  #     # intelliJ will output the build result in /out/production/BurrowsWheeler
  java -cp ./out/production/BurrowsWheeler/:../lib/algs4.jar BurrowsWheeler - < testData/burrows/abra.txt | java -cp ./out/production/BurrowsWheeler/:../lib/algs4.jar BurrowsWheeler +
fi

if [[ $1 == testOnlyDecodeBW ]]; then
  echo "test the only decoding Burrows-Wheeler"
  #     # intelliJ will output the build result in /out/production/BurrowsWheeler
  java -cp ./out/production/BurrowsWheeler/:../lib/algs4.jar BurrowsWheeler + < testData/burrows/us.gif.bwt | java -cp ../lib/algs4.jar edu.princeton.cs.algs4.HexDump 16
fi
