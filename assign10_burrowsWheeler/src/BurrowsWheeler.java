import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;
import edu.princeton.cs.algs4.Queue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by lor on 06.12.2015.
 */
public class BurrowsWheeler {
  // apply Burrows-Wheeler encoding, reading from standard input and writing to standard output
  public static void encode() {
    // read the suffix
    String suffix = BinaryStdIn.readString();
    CircularSuffixArray ca = new CircularSuffixArray(suffix);

    String word = suffix;
    int length = suffix.length();
    int first = -1;
    char[] lastColumn = new char[length];

    for (int i = 0; i < length; i++) {
      int index = ca.index(i);
      if (index == 0) first = i;
      int res = (length - 1 + index < length) ? (length - 1 + index) : (length - 1 + index - length);
      lastColumn[i] = word.charAt(res);
    }

    BinaryStdOut.write(first);
    BinaryStdOut.flush();

    for (int i = 0; i < length; i++) {
      BinaryStdOut.write((byte) lastColumn[i]);
      BinaryStdOut.flush();
    }

    BinaryStdOut.close();
  }

  // apply Burrows-Wheeler decoding, reading from standard input and writing to standard output
  public static void decode() {
    int first = BinaryStdIn.readInt();

    char[] lastColumn = new char[2];
    int length = 0;
    while (!BinaryStdIn.isEmpty()) {
      if (length == lastColumn.length) {
        lastColumn = Arrays.copyOf(lastColumn, length * 2);
      }

      lastColumn[length] = BinaryStdIn.readChar();
      length++;
    }

    lastColumn = Arrays.copyOf(lastColumn, length);
    char[] firstColumn = Arrays.copyOf(lastColumn, length); //s.toCharArray();
    Arrays.sort(firstColumn);
    // until here is fine

    // create the next array
    int[] next = new int[lastColumn.length];

    // use count sort
    HashMap<Character, Queue<Integer>> lastResort = new HashMap<>();
    for (int i = 0; i < lastColumn.length; i++) {
      Character c = lastColumn[i];

      if (!lastResort.containsKey(c)) lastResort.put(c, new Queue<>());
      lastResort.get(c).enqueue(i);
    }

    for (int i = 0; i < firstColumn.length; i++) {
      Character key = firstColumn[i];
      next[i] = lastResort.get(key).dequeue();
    }

    BinaryStdOut.write(firstColumn[first]);
    BinaryStdOut.flush();
    for (int i = 1; i < next.length; i++) {
      BinaryStdOut.write(firstColumn[next[first]]);
      BinaryStdOut.flush();
      first = next[first];
    }

    BinaryStdOut.close();
  }

  // if args[0] is '-', apply Burrows-Wheeler encoding
  // if args[0] is '+', apply Burrows-Wheeler decoding
  public static void main(String[] args) {
    if (args[0].equals("-")) {
      encode();
    }

    if (args[0].equals("+")) {
      decode();
    }
  }
}
