import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by lor on 06.12.2015.
 */
public class CircularSuffixArray {
  private int length;
  private String suffix;
  private Integer[] originalSuffixes;
  private Integer[] sortedSuffixes;

  public CircularSuffixArray(String s) {
    if (s == null) throw new NullPointerException("argument to circular suffix array is null");

    length = s.length();
    suffix = s;

    creteOriginalSuffixes();
    createSortedSuffixes();
  }

  private void creteOriginalSuffixes() {
    originalSuffixes = new Integer[length];
    for (int i = 0; i < length; i++) originalSuffixes[i] = i;
  }

  private void createSortedSuffixes() {
    // use a radix sort here
    sortedSuffixes = new Integer[length];
    for (int i = 0; i < length; i++) sortedSuffixes[i] = i;
    Arrays.sort(sortedSuffixes, getSuffixComparator());
  }

  private Comparator<Integer> getSuffixComparator() {
    return (o1, o2) -> {
      for (int i = 0; i < length; i++) {
        char a = (i + o1 < length) ? suffix.charAt(i + o1) : suffix.charAt(i + o1 - length);
        char b = (i + o2 < length) ? suffix.charAt(i + o2) : suffix.charAt(i + o2 - length);

        int res = Character.compare(a, b);
        if (res != 0) return res;
      }

      return 0;
    };
  }

  public int length() { return length; }

  public int index(int i) {
    if (i < 0 || i >= length) throw new IndexOutOfBoundsException("i is outside the range");

    return sortedSuffixes[i];
  }

  private String showSuffixStartingAt(int index) {
    char[] ret = new char[length];

    for (int i = 0; i < length; i++) {
      ret[i] = (i + index < length) ? suffix.charAt(i + index) : suffix.charAt(i + index - length);
    }

    return new String(ret);
  }
}
