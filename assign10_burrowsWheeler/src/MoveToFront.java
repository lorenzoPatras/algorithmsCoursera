import edu.princeton.cs.algs4.BinaryStdIn;
import edu.princeton.cs.algs4.BinaryStdOut;

/**
 * Created by lor on 06.12.2015.
 */
public class MoveToFront {
  // apply move-to-front encoding, reading from standard input and writing to standard output
  public static void encode() {
    // init the table
    byte[] table = new byte[256];
    for (int i = 0; i < 256; i++) table[i] = (byte) i;

    while (!BinaryStdIn.isEmpty()) {
      byte c = BinaryStdIn.readByte();

      for (int i = 0; i < 256; i++) {
        if (table[i] == c) {
          // write index
          BinaryStdOut.write((byte) i);
          BinaryStdOut.flush();

          // rotate to right all elements until index
          for (int j = i; j > 0; j--) {
            table[j] = table[j-1];
          }
          table[0] = c;
          break;
        }
      }
    }
    BinaryStdOut.flush();
    BinaryStdOut.close();
  }

  // apply move-to-front decoding, reading from standard input and writing to standard output
  public static void decode() {
    // init the table
    byte[] table = new byte[256];
    for (int i = 0; i < 256; i++) table[i] = (byte) i;

    while (!BinaryStdIn.isEmpty()) {
      int index = (BinaryStdIn.readByte() & 0xff);
      byte c = table[index];

      // write char at index
      BinaryStdOut.write(c);
      BinaryStdOut.flush();

      // rotate to right all elements until index
      for (int j = index; j > 0; j--) {
        table[j] = table[j-1];
      }

      table[0] = c;
    }
    BinaryStdOut.flush();
    BinaryStdOut.close();
  }

  // if args[0] is '-', apply move-to-front encoding
  // if args[0] is '+', apply move-to-front decoding
  public static void main(String[] args) {
    if (args[0].equals("-")) {
      encode();
    }

    if (args[0].equals("+")) {
      decode();
    }
  }
}
