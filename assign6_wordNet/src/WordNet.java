import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.DirectedCycle;
import edu.princeton.cs.algs4.In;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by lorenzo on 02.11.2015.
 */
public class WordNet {
  private DoubleWayHash network = new DoubleWayHash();
  private Digraph networkDigraph;
  private SAP sap;

  // constructor takes the name of the two input files
  public WordNet(String synsets, String hypernyms) {
    if (synsets == null || hypernyms == null) throw new NullPointerException("null args");

    int maxInt = 0;
    In synsetsFile = new In(synsets);
    while (synsetsFile.hasNextLine()) {
      String currentLine = synsetsFile.readLine();
      String[] elements = currentLine.split(",");
      String[] synset = elements[1].split(" ");
      int crtId = Integer.parseInt(elements[0]);
      if (crtId > maxInt) maxInt = crtId;

      for (String s : synset) {
        network.insertEntry(crtId, s);
      }
    }

    networkDigraph = new Digraph(maxInt + 1);
    In hypernymsFile = new In(hypernyms);
    while (hypernymsFile.hasNextLine()) {
      String line = hypernymsFile.readLine();
      String[] elements = line.split(",");
      for (int i = 1; i < elements.length; i++) {
        networkDigraph.addEdge(Integer.parseInt(elements[0]), Integer.parseInt(elements[i]));
      }
    }

    checkRoutedDAG();

    sap = new SAP(networkDigraph);
  }

  private void checkRoutedDAG() {
    int roots = 0;

    for (int i = 0; i < networkDigraph.V(); i++) {
      if (networkDigraph.outdegree(i) == 0) roots++;
    }

    DirectedCycle dc = new DirectedCycle(networkDigraph);

    if (roots > 1 || dc.hasCycle()) throw new IllegalArgumentException("the digraph has a cycle");
  }

  // returns all WordNet nouns
  public Iterable<String> nouns() {
    return network.getAllNouns();
  }

  // is the word a WordNet noun?
  public boolean isNoun(String word) {
    if (word == null) throw new NullPointerException("null arg");

    return network.getId(word) != null;
  }

  // distance between nounA and nounB (defined below)
  public int distance(String nounA, String nounB) {
    if (nounA == null || nounB == null) throw new NullPointerException("null args");
    if (!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException("not nouns");

    return sap.length(network.getId(nounA), network.getId(nounB));
  }

  // a synset (second field of synsets.txt) that is the common ancestor of nounA and nounB
  // in a shortest ancestral path (defined below)
  public String sap(String nounA, String nounB) {
    if (nounA == null || nounB == null) throw new NullPointerException("null args");
    if (!isNoun(nounA) || !isNoun(nounB)) throw new IllegalArgumentException("not nouns");

    int ancestorIndex = sap.ancestor(network.getId(nounA), network.getId(nounB));

    ArrayList<String> syns = network.getSynsets(ancestorIndex);

    String res = "";

    for (String s : syns) {
      res.concat(s + " ");
    }

    return res;
  }

  private class DoubleWayHash {
    private HashMap<Integer, ArrayList<String>> idToSynsetHash = new HashMap<>();
    private HashMap<String, ArrayList<Integer>> nounToIntHash = new HashMap<>();

    public void insertEntry(int id, String noun) {
      insertNoun(id, noun);
      insertId(noun, id);
    }

    private void insertNoun(int id, String noun) {
      if (!idToSynsetHash.containsKey(id)) idToSynsetHash.put(id, new ArrayList<String>());

      idToSynsetHash.get(id).add(noun);
    }

    private void insertId(String noun, int id) {
      if (!nounToIntHash.containsKey(noun)) nounToIntHash.put(noun, new ArrayList<Integer>());

      nounToIntHash.get(noun).add(id);
    }

    public ArrayList<String> getSynsets(int id) { return idToSynsetHash.get(id); }

    public ArrayList<Integer> getId(String noun) {
      return nounToIntHash.get(noun);
    }

    public ArrayList<String> getAllNouns() {
      return new ArrayList<String>(nounToIntHash.keySet());
    }
  }
}
