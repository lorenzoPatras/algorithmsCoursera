import edu.princeton.cs.algs4.In;

/**
 * Created by lorenzo on 02.11.2015.
 */
public class Outcast {
  private WordNet wordnet;

  public Outcast(WordNet wordnet) {
    this.wordnet = wordnet;
  }

  // given an array of WordNet nouns, return an outcast
  public String outcast(String[] nouns) {
    int maxDist = 0;
    String ret = "";
    for (String source : nouns) {
      int localDist = 0;
      for (String dest : nouns) {
        localDist += wordnet.distance(source, dest);
      }

      if (localDist > maxDist) {
        maxDist = localDist;
        ret = source;
      }
    }


    return ret;
  }
}
