import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;

/**
 * Created by lorenzo on 02.11.2015.
 */
public class SAP {
  private Digraph digraph;



  public SAP(Digraph G) {
    if (G == null) throw new NullPointerException("null param");

    digraph = new Digraph(G);
  }

  // length of shortest ancestral path between v and w; -1 if no such path
  public int length(int v, int w) {
    validateVertex(v);
    validateVertex(w);

    BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
    BreadthFirstDirectedPaths bfsW = new BreadthFirstDirectedPaths(digraph, w);

    int minDistance = Integer.MAX_VALUE;
    for (int i = 0; i < digraph.V(); i++) {
      if (bfsV.distTo(i) != Integer.MAX_VALUE && bfsW.distTo(i) != Integer.MAX_VALUE) {
        if (minDistance > bfsV.distTo(i) + bfsW.distTo(i)) minDistance = bfsV.distTo(i) + bfsW.distTo(i);
      }
    }

    return minDistance == Integer.MAX_VALUE ? -1 : minDistance;
  }

  // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
  public int ancestor(int v, int w) {
    validateVertex(v);
    validateVertex(w);

    BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
    BreadthFirstDirectedPaths bfsW = new BreadthFirstDirectedPaths(digraph, w);

    int minDistance = Integer.MAX_VALUE;
    int ancestor = -1;
    for (int i = 0; i < digraph.V(); i++) {
      if (bfsV.distTo(i) != Integer.MAX_VALUE &&
              bfsW.distTo(i) != Integer.MAX_VALUE &&
              minDistance > bfsV.distTo(i) + bfsW.distTo(i)) {
        minDistance = bfsV.distTo(i) + bfsW.distTo(i);
        ancestor = i;
      }
    }

    return ancestor;
  }

  // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
  public int length(Iterable<Integer> v, Iterable<Integer> w) {
    if (v == null || w == null) throw new NullPointerException("null param");

    BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
    BreadthFirstDirectedPaths bfsW = new BreadthFirstDirectedPaths(digraph, w);

    int minDistance = Integer.MAX_VALUE;
    for (int i = 0; i < digraph.V(); i++) {
      if (bfsV.distTo(i) != Integer.MAX_VALUE && bfsW.distTo(i) != Integer.MAX_VALUE) {
        if (minDistance > bfsV.distTo(i) + bfsW.distTo(i)) minDistance = bfsV.distTo(i) + bfsW.distTo(i);
      }
    }

    return minDistance == Integer.MAX_VALUE ? -1 : minDistance;
  }

  // a common ancestor that participates in shortest ancestral path; -1 if no such path
  public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
    if (v == null || w == null) throw new NullPointerException("null param");

    BreadthFirstDirectedPaths bfsV = new BreadthFirstDirectedPaths(digraph, v);
    BreadthFirstDirectedPaths bfsW = new BreadthFirstDirectedPaths(digraph, w);

    int minDistance = Integer.MAX_VALUE;
    int ancestor = -1;
    for (int i = 0; i < digraph.V(); i++) {
      if (bfsV.distTo(i) != Integer.MAX_VALUE &&
              bfsW.distTo(i) != Integer.MAX_VALUE &&
              minDistance > bfsV.distTo(i) + bfsW.distTo(i)) {
        minDistance = bfsV.distTo(i) + bfsW.distTo(i);
        ancestor = i;
      }
    }

    return ancestor;
  }

  private void validateVertex(int v) {
    if (v < 0 || v >= digraph.V())
      throw new IndexOutOfBoundsException("vertex out of bounds");
  }
}
