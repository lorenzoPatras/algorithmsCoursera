#include <iostream>

#include "StackList.h"
#include "StackArray.h"

using namespace std;


int main() {
  // test stack with linked list
  StackList<int> sl;

  if (sl.isEmpty()) cout << "empty" << endl;
  else cout << "not empty" << endl;

  sl.push(0);
  sl.push(2);
  sl.push(3);

  if (sl.isEmpty()) cout << "empty" << endl;
  else cout << "not empty" << endl;

  cout << sl.pop() << endl;
  cout << sl.pop() << endl;
  cout << sl.pop() << endl;

  if (sl.isEmpty()) cout << "empty" << endl;
  else cout << "Wrong!!! not empty" << endl;

  try {
    sl.pop();
  } catch(exception ex) {
    cout << ex.what() << endl;
  }

  // test stack with vector
  StackArray<int> sa;
  if (sa.isEmpty()) cout << "empty" << endl;
  else cout << "not empty" << endl;
  
  sa.push(0);
  sa.push(2);
  sa.push(3);
  
  if (sa.isEmpty()) cout << "empty" << endl;
  else cout << "not empty" << endl;
   
  cout << sa.pop() << endl;
  cout << sa.pop() << endl;
  cout << sa.pop() << endl;
   
  if (sa.isEmpty()) cout << "empty" << endl;
  else cout << "not empty" << endl;

  try {
    sl.pop();
  } catch(exception ex) {
    cout << ex.what() << endl;
  }

  return 0;
}