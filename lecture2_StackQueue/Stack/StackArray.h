#ifndef STACK_ARRAY_H
#define STACK_ARRAY_H

#include <stdexcept>

#include "Stack.h"
#include <vector>

using namespace std;

template <typename T>
class StackArray : public Stack<T> {
public:
  StackArray();
  ~StackArray();
  virtual void push(T newElem);
  virtual T pop();
  virtual bool isEmpty();

private:
  vector<T> elems;
  int headIndex;
};

template <typename T>
StackArray<T>::StackArray() {
  headIndex = -1;
  elems.resize(2);
}

template <typename T>
StackArray<T>::~StackArray() {}

template <typename T>
void StackArray<T>::push(T newElem) {
  headIndex++;
  if (headIndex == elems.size()) {
    elems.resize(elems.size() * 2);
  }

  elems[headIndex] = newElem;
}

template <typename T>
T StackArray<T>::pop() {
  if (headIndex < 0) throw exception("Cannot pop: stack is empty");

  T ret = elems[headIndex--];

  if (headIndex > 0 && headIndex == elems.size() / 4) {
    elems.resize(elems.size() / 2);
  }


  return ret;
}

template <typename T>
bool StackArray<T>::isEmpty() {
  return headIndex == -1;
}

#endif
