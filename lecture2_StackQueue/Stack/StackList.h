#ifndef STACK_LIST_H
#define STACK_LIST_H

#include <stdexcept>

#include "Stack.h"

using namespace std;

template <typename T>
class Node {
public:
  Node<T>() {data = T(); next = nullptr;}
  Node<T>(T data, shared_ptr<Node<T>> next) : data(data), next(next) {}

  T data;
  shared_ptr<Node<T>> next;
};

template <typename T>
class StackList : public Stack<T> {
public:
  StackList();
  ~StackList();
  virtual void push(T newElem);
  virtual T pop();
  virtual bool isEmpty();

private:
  shared_ptr<Node<T>> head;
};

template <typename T>
StackList<T>::StackList() {}

template <typename T>
StackList<T>::~StackList() {}

template <typename T>
void StackList<T>::push(T newElem) {
  if (head == nullptr) {
    head = shared_ptr<Node<T>>(new Node<T>(newElem, nullptr));
  } else {
    shared_ptr<Node<T>> newNode = shared_ptr<Node<T>>(new Node<T>(newElem, head));
    head = newNode;
  }
}

template <typename T>
T StackList<T>::pop() {
  if (head == nullptr) throw exception("Cannot pop: stack is empty");

  T ret = head->data;
  head = head->next;
  return ret;
}

template <typename T>
bool StackList<T>::isEmpty() {
  return head == nullptr;
}

#endif
