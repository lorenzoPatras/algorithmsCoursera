#ifndef STACK_H
#define STACK_H

#include <memory>

using namespace std;


template <typename T>
class Stack {
public:
  Stack();
  virtual ~Stack()              = 0;
  virtual void push(T newElem)  = 0;
  virtual T pop()               = 0;
  virtual bool isEmpty()        = 0;
};

template <typename T>
Stack<T>::Stack() {}

template <typename T>
Stack<T>::~Stack() {}

#endif