import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.TrieST;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by lor on 03.12.2015.
 */
public class BoggleSolver
{

  private Trie<Integer> dictionary = new Trie<>();
  private HashSet<String> words = new HashSet<>();
  private BoggleBoard board;
  private boolean[][] visited;

  // Initializes the data structure using the given array of strings as the dictionary.
  // (You can assume each word in the dictionary contains only the uppercase letters A through Z.)
  public BoggleSolver(String[] dictionary) {
    for (String s : dictionary) {
      this.dictionary.put(s, getValueForWord(s));
    }
  }

  private int getValueForWord(String s) {
    int ret;

    if (s.length() < 3) ret = 0;
    else if (s.length() < 5) ret = 1;
    else if (s.length() == 5) ret = 2;
    else if (s.length() == 6) ret = 3;
    else if (s.length() == 7) ret = 5;
    else ret = 11;

    for (int i = 0; i < s.length() - 1; i++) {
      if (s.charAt(i) == 'Q' && s.charAt(i + 1) != 'U') ret++;
    }

    return ret;
  }

  // Returns the set of all valid words in the given Boggle board, as an Iterable.
  public Iterable<String> getAllValidWords(BoggleBoard board) {
    this.board = board;
    words.clear();

    for (int i = 0; i < board.rows(); i++) {
      for (int j = 0; j < board.cols(); j++) {
        visited = new boolean[board.rows()][board.cols()];
        exploreBoardStartingFrom(i, j);
      }
    }

    return new ArrayList<>(words);
  }

  private void exploreBoardStartingFrom(int i, int j) {
    String firstLetter = String.valueOf(board.getLetter(i, j));
    if (firstLetter.equals("Q")) firstLetter += "U";

    visited[i][j] = true;
    for (int iOffset = -1; iOffset <= 1; iOffset++) {
      for (int jOffset = -1; jOffset <= 1; jOffset++) {
        if (!isOutside(i + iOffset, j + jOffset) && !visited[i + iOffset][j + jOffset]) {
          expandSearch(i + iOffset, j + jOffset, firstLetter);
          visited[i + iOffset][j + jOffset] = false;
        }
      }
    }
  }

  private void expandSearch(int i, int j, String prefix) {
    String append = String.valueOf(board.getLetter(i, j));
    if (append.equals("Q")) append += "U";
    prefix += append;

    if (dictionary.contains(prefix) && prefix.length() >= 3) words.add(prefix);

    if (((Queue<String>) dictionary.keysWithPrefix(prefix)).isEmpty()) {
      return;
    }

    visited[i][j] = true;

    for (int iOffset = -1; iOffset <= 1; iOffset++) {
      for (int jOffset = -1; jOffset <= 1; jOffset++) {
        if (!isOutside(i + iOffset, j + jOffset) && !visited[i + iOffset][j + jOffset]) {
          expandSearch(i + iOffset, j + jOffset, prefix);
          visited[i + iOffset][j + jOffset] = false;
        }
      }
    }
  }

  private boolean isOutside(int i, int j) {
    if (i < 0 || i >= board.rows() || j < 0 || j >= board.cols()) return true;
    return false;
  }

  // Returns the score of the given word if it is in the dictionary, zero otherwise.
  // (You can assume the word contains only the uppercase letters A through Z.)
  public int scoreOf(String word) {
    return dictionary.contains(word) ? dictionary.get(word) : 0;
  }


  public static void main(String[] args)
  {
    In in = new In("dictionary-16q.txt");
    String[] dictionary = in.readAllStrings();
    BoggleSolver solver = new BoggleSolver(dictionary);
    BoggleBoard board = new BoggleBoard("board-16q.txt");
    long startTime = System.currentTimeMillis();


    int score = 0;
    for (String word : solver.getAllValidWords(board))
    {
      StdOut.println(word + " " + solver.scoreOf(word));
      score += solver.scoreOf(word);
    }
    StdOut.println("Score = " + score);

    long stopTime = System.currentTimeMillis();
    long elapsedTime = stopTime - startTime;
    System.out.println("time: " + elapsedTime + " ms");
  }

  private class Trie<Value> {
    private static final int R = 26;
    private Node root;      // root of trie
    private int N;          // number of keys in trie


    private class Node {
      private Object val;
      private Node[] next = (Node[]) Array.newInstance(Node.class, R);
    }

    public Value get(String key) {
      Node x = get(root, key, 0);
      if (x == null) return null;
      return (Value) x.val;
    }

    private Node get(Node x, String key, int d) {
      if (x == null) return null;
      if (d == key.length()) return x;
      char c = key.charAt(d);
      return get(x.next[c - 65], key, d+1);
    }

    public boolean contains(String key) {
      return get(key) != null;
    }

    public void put(String key, Value val) {
      root = put(root, key, val, 0);
    }

    private Node put(Node x, String key, Value val, int d) {
      if (x == null) x = new Node();
      if (d == key.length()) {
        if (x.val == null) N++;
        x.val = val;
        return x;
      }
      char c = key.charAt(d);
      x.next[c - 65] = put(x.next[c - 65], key, val, d+1);
      return x;
    }

    public Iterable<String> keysWithPrefix(String prefix) {
      Queue<String> results = new Queue<String>();
      Node x = get(root, prefix, 0);
      collect(x, new StringBuilder(prefix), results);
      return results;
    }

    private void collect(Node x, StringBuilder prefix, Queue<String> results) {
      if (x == null) return;
      if (x.val != null) {
        results.enqueue(prefix.toString()); // comment this to get all prefixes. needed to fasten the computation time
        return;
      }
      for (char c = 0; c < R; c++) {
        prefix.append(c);
        collect(x.next[c], prefix, results);
        prefix.deleteCharAt(prefix.length() - 1);
      }
    }
  }
}