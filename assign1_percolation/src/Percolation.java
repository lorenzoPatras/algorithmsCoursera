import edu.princeton.cs.algs4.WeightedQuickUnionUF;


public class Percolation {
  private static final int VIRTUAL_ELEMENTS_NR = 2;
  private int N;
  private int virtualTop;
  private int virtualBottom;
  private boolean[][] isOpenSite;
  private WeightedQuickUnionUF unionFind;
  private WeightedQuickUnionUF backwashPreventingUF;

  public Percolation(int N) {
    if (N <= 0) throw new IllegalArgumentException();

    this.N = N;
    virtualTop = 0;
    virtualBottom = N * N + 1;
    isOpenSite = new boolean[N][N];
    unionFind = new WeightedQuickUnionUF(N * N + VIRTUAL_ELEMENTS_NR);
    backwashPreventingUF = new WeightedQuickUnionUF(N * N + 1);
  }

  // open site [i,j]
  public void open(int i, int j) {
    checkCoordinates(i, j);

    isOpenSite[i - 1][j - 1] = true;
    connectToNeighbours(i - 1, j - 1); // decrement for an easier indexing in the unionFind internal array
  }

  private void connectToNeighbours(int i, int j) {
    connectToTopNeighbour(i, j);
    connectToBottomNeighbour(i, j);
    connectToRightNeighbour(i, j);
    connectToLeftNeighbour(i, j);
  }

  private void connectToTopNeighbour(int i, int j) {
    if (i == 0) {
      unionFind.union(virtualTop, transformArrayToUFCoordinates(i, j));
      backwashPreventingUF.union(virtualTop, transformArrayToUFCoordinates(i, j));
    }
    else if (isOpenSite[i - 1][j]) {
      unionFind.union(transformArrayToUFCoordinates(i - 1, j), transformArrayToUFCoordinates(i, j));
      backwashPreventingUF.union(transformArrayToUFCoordinates(i - 1, j), transformArrayToUFCoordinates(i, j));
    }
  }

  private void connectToBottomNeighbour(int i, int j) {
    if (i == N - 1) unionFind.union(virtualBottom, transformArrayToUFCoordinates(i, j));
    else if (isOpenSite[i + 1][j]) {
      unionFind.union(transformArrayToUFCoordinates(i, j), transformArrayToUFCoordinates(i + 1, j));
      backwashPreventingUF.union(transformArrayToUFCoordinates(i, j), transformArrayToUFCoordinates(i + 1, j));
    }
  }

  private void connectToRightNeighbour(int i, int j) {
    if (j == N - 1) return;
    if (isOpenSite[i][j + 1]) {
      unionFind.union(transformArrayToUFCoordinates(i, j + 1), transformArrayToUFCoordinates(i, j));
      backwashPreventingUF.union(transformArrayToUFCoordinates(i, j + 1), transformArrayToUFCoordinates(i, j));
    }
  }

  private void connectToLeftNeighbour(int i, int j) {
    if (j == 0) return;
    if (isOpenSite[i][j - 1]) {
      unionFind.union(transformArrayToUFCoordinates(i, j - 1), transformArrayToUFCoordinates(i, j));
      backwashPreventingUF.union(transformArrayToUFCoordinates(i, j - 1), transformArrayToUFCoordinates(i, j));
    }
  }

  // Returns true if the site[i][j] is open. I and J must be decremented due to convention of indices <- [1..N]
  public boolean isOpen(int i, int j) {
    checkCoordinates(i, j);

    return isOpenSite[i - 1][j - 1];
  }

  // Returns true if there is a flowing path from the top row to the current cell.
  public boolean isFull(int i, int j) {
    checkCoordinates(i, j);

    return backwashPreventingUF.connected(virtualTop, transform2Dto1D(i, j));
  }

  public boolean percolates() {
    return unionFind.connected(virtualTop, virtualBottom);
  }

  private void checkCoordinates(int i, int j) {
    if (isOutsideRange(i, j)) throw new IndexOutOfBoundsException();
  }

  private boolean isOutsideRange(int i, int j) {
    return (i < 1 || i > N || j < 1 || j > N);
  }

  private int transform2Dto1D(int i, int j) {
    return (i - 1) * N + j;
  }

  private int transformArrayToUFCoordinates(int i, int j) {
    return transform2Dto1D(i + 1, j + 1);
  }
}
