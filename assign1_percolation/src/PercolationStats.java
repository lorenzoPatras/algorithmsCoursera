import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

public class PercolationStats {
  private double[] openSitesPercentage;
  private int nrOfExperiments;
  private int N;

  // perform T independent experiments on an N-by-N grid
  public PercolationStats(int N, int T) {
    if (N <= 0 || T <= 0) throw new IllegalArgumentException();

    nrOfExperiments = T;
    this.N = N;
    openSitesPercentage = new double[T];

    performExperiments();
  }

  private void performExperiments() {
    for (int i = 0; i < nrOfExperiments; i++) {
      Percolation p = new Percolation(N);

      while (!p.percolates()) {
        int x = StdRandom.uniform(N) + 1; // returns a nr between [1..N]
        int y = StdRandom.uniform(N) + 1;

        if (!p.isOpen(x, y)) {
          p.open(x, y);
          openSitesPercentage[i]++;
        }
      }

      openSitesPercentage[i] /= N * N;
    }
  }

  // sample mean of percolation threshold
  public double mean() {
    return StdStats.mean(openSitesPercentage);
  }

  // sample standard deviation of percolation threshold
  public double stddev() {
    return StdStats.stddev(openSitesPercentage);
  }

  // low  endpoint of 95% confidence interval
  public double confidenceLo() {
    return mean() - 1.96 * stddev() / Math.sqrt(nrOfExperiments);
  }

  // high endpoint of 95% confidence interval
  public double confidenceHi() {
    return mean() + 1.96 * stddev() / Math.sqrt(nrOfExperiments);
  }

  public static void main(String[] args) {
    int N = Integer.parseInt(args[0]);
    int nrOfExperiments = Integer.parseInt(args[1]);

    PercolationStats stats  = new PercolationStats(N, nrOfExperiments);

    System.out.println("mean                    = " + stats.mean());
    System.out.println("stddev                  = " + stats.stddev());
    System.out.println("95% confidence interval = " + stats.confidenceLo() + " " + stats.confidenceHi());
  }
}
