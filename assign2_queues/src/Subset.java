import edu.princeton.cs.algs4.StdIn;

/**
 * Created by lorenzo on 27.09.2015.
 */
public class Subset {
  public static void main(String[] args) {
    int k = Integer.parseInt(args[0]);
    RandomizedQueue<String> stringsQ = new RandomizedQueue<>();

    while (!StdIn.isEmpty()) {
      stringsQ.enqueue(StdIn.readString());
    }

    while (k > 0) {
      System.out.println(stringsQ.dequeue());

      k--;
    }
  }

}
