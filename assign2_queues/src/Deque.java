import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by lorenzo on 26.09.2015.
 */
public class Deque<Item> implements Iterable<Item> {
  private Node first;
  private Node last;
  private int size;

  public Deque() { }

  public boolean isEmpty() {
    return size == 0;
  }

  public int size() {
    return size;
  }

  // add item at the front of the queue
  public void addFirst(Item item) {
    if (item == null) throw new NullPointerException();

    Node oldFirst = first;
    first = new Node(item);
    first.next = oldFirst;

    if (oldFirst != null)
      oldFirst.prev = first;

    if (size == 0) last = first;

    size++;
  }

  // add element at the end of the queue
  public void addLast(Item item) {
    if (item == null) throw new NullPointerException();

    Node oldLast = last;
    last = new Node(item);
    last.prev = oldLast;

    if (oldLast != null)
      oldLast.next = last;

    if (size == 0) first = last;

    size++;
  }

  // remove element from the front of the queue
  public Item removeFirst() {
    if (isEmpty()) throw new NoSuchElementException();

    Item ret = first.item;
    first = first.next;

    if (first != null) first.prev = null;

    if (--size == 0) last = null;

    return ret;
  }

  // remove element from the end of the queue
  public Item removeLast() {
    if (isEmpty()) throw new NoSuchElementException();

    Item ret = last.item;
    last = last.prev;

    if (last != null) last.next = null;

    if (--size == 0) first = null;

    return ret;
  }

  @Override
  public Iterator<Item> iterator() {
    return new DequeIterator();
  }

  private class Node {
    private Item item;
    private Node next;
    private Node prev;

    public Node(Item item) {
      this.item = item;
    }
  }

  private class DequeIterator implements Iterator<Item> {
    private Node current = first;

    @Override
    public boolean hasNext() {
      return current != null;
    }

    @Override
    public Item next() {
      if (current == null) throw new NoSuchElementException();

      Item item = current.item;
      current = current.next;
      return item;
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
}
