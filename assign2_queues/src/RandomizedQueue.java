import edu.princeton.cs.algs4.StdRandom;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by lorenzo on 27.09.2015.
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
  private int size;
  private Item[] items;

  public RandomizedQueue() {
    items = (Item[]) new Object[2];
  }

  public boolean isEmpty() {
    return size == 0;
  }

  public int size() {
    return size;
  }

  private void resize(int capacity) {
    assert capacity >= size;
    Item[] temp = (Item[]) new Object[capacity];

    for (int i = 0; i < size; i++) {
      temp[i] = items[i];
    }

    items = temp;
  }

  public void enqueue(Item item) {
    if (item == null) throw new NullPointerException();

    if (size == items.length) resize(items.length * 2);

    items[size++] = item;
  }

  // remove and return a random item
  public Item dequeue() {
    if (isEmpty()) throw new NoSuchElementException();

    int rand = StdRandom.uniform(size);

    Item item = items[rand];
    items[rand] = items[--size];
    items[size] = null;

    if (size > 0 && size == items.length/4) resize(items.length/2);

    return item;
  }

  // return but do not remove a random item
  public Item sample() {
    if (isEmpty()) throw new NoSuchElementException();
    return items[StdRandom.uniform(size)];
  }

  @Override
  public Iterator<Item> iterator() {
    return new RandQueIterator(items, size);
  }

  private class RandQueIterator implements Iterator<Item> {
    private int i;
    private Item[] shuffledArray;

    public RandQueIterator(Item[] originalArray, int size) {
      shuffledArray = Arrays.copyOf(originalArray, size);
      i = size - 1;
      StdRandom.shuffle(shuffledArray);
    }

    @Override
    public boolean hasNext() {
      return i >= 0;
    }

    @Override
    public Item next() {
      if (!hasNext()) throw new NoSuchElementException();

      return shuffledArray[i--];
    }

    @Override
    public void remove() {
      throw new UnsupportedOperationException();
    }
  }
}