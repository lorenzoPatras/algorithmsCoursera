import edu.princeton.cs.algs4.Picture;

/**
 * Created by lorenzo on 13.11.2015.
 */
public class SeamCarver {
  private Picture picture;
  //private double[][] costMatrix;
  private double[][] energyMatrix;


  public SeamCarver(Picture picture) {
    checkNull(picture);

    this.picture = new Picture(picture);
  }

  public Picture picture() {
    return new Picture(picture);
  }

  public int width() {
    return picture.width();
  }

  public int height() {
    return picture.height();
  }

  public double energy(int x, int y) {
    checkOutsideOfRange(x, y);

    if (isBorderPixel(x, y)) return 1000;

    return computeDualGradientEnergy(x, y);
  }

  public int[] findHorizontalSeam() {
    transposePicture();
    int[] horizontalSeam = findVerticalSeam();
    transposePicture();

    return horizontalSeam;
  }

  public int[] findVerticalSeam() {
    constructEnergyMatrix();
    computeCostMatrix();

    return getSeam(getFinalColumn());
  }

  private void constructEnergyMatrix() {
    energyMatrix = new double[picture.width()][picture.height()];

    for (int col = 0; col < picture.width(); col++) {
      for (int row = 0; row < picture.height(); row++)
        energyMatrix[col][row] = energy(col, row);
    }
  }

  private void computeCostMatrix() {
    for (int row = 1; row < picture.height(); row++) {
      for (int col = 0; col < picture.width(); col++) {
        energyMatrix[col][row] += getMinNeighbourCost(col, row);
      }
    }
  }

  private double getMinNeighbourCost(int col, int row) {
    row--; // explore prev row
    double min = energyMatrix[col][row];// we know that this one really exists

    for (int i = -1; i <= 1; i++) {// check the left and right columns
      if (col + i < 0 || col + i >= picture.width()) continue;

      if (energyMatrix[col + i][row] < min)  min = energyMatrix[col + i][row];
    }

    return min;
  }


  private int[] getSeam(int col) {
    int[] ret = new int[picture.height()];

    for (int row = picture.height() - 1; row > 0; row--) {
      ret[row] = col;
      col = getMinNeighbourColumn(col, row);
    }

    ret[0] = col;

    return ret;
  }

  private int getMinNeighbourColumn(int col, int row) {
    row--; // explore prev row
    double min = energyMatrix[col][row];// we know that this one really exists
    int ret = col;

    for (int i = -1; i <= 1; i++) {
      if (col + i < 0 || col + i >= picture.width()) continue;

      if (energyMatrix[col + i][row] < min) {
        min = energyMatrix[col + i][row];
        ret = col + i;
      }
    }

    return ret;
  }

  private int getFinalColumn() {
    double min = Double.POSITIVE_INFINITY;
    int finalCol = -1;
    for (int col = 0; col < picture.width(); col++) {
      if (energyMatrix[col][picture.height() - 1] < min) {
        min = energyMatrix[col][picture.height() - 1];
        finalCol = col;
      }
    }

    return finalCol;
  }

  public void removeHorizontalSeam(int[] seam) {
    transposePicture();
    removeVerticalSeam(seam);
    transposePicture();
  }

  public void removeVerticalSeam(int[] seam) {
    checkNull(seam);
    checkPictureSize(picture.width());
    checkSeamLength(seam.length);
    checkSeamConsistency(seam);

    for (int row = 0; row < picture.height(); row++) {
      for (int col = seam[row]; col < picture.width() - 1; col++)
        picture.set(col, row, picture.get(col + 1, row));
    }

    // remove last row from picture
    Picture newPic = new Picture(picture.width() - 1, picture.height());
    for (int col = 0; col < newPic.width(); col++) {
      for (int row = 0; row < newPic.height(); row++)
        newPic.set(col, row, picture.get(col, row));
    }

    picture = newPic;
  }

  private void transposePicture() {
    Picture tempPic = new Picture(picture.height(), picture.width());
    for (int col = 0; col < picture.width(); col++) {
      for (int row = 0; row < picture.height(); row++) {
        tempPic.set(row, col, picture.get(col, row));
      }
    }

    picture = tempPic;
  }

  private void checkOutsideOfRange(int x, int y) {
    if (x < 0 || x >= picture.width() || y < 0 || y >= picture.height())
      throw new IndexOutOfBoundsException("an index is out of bounds");
  }

  private void checkNull(Object obj) {
    if (obj == null) throw new NullPointerException("argument is null");
  }

  private void checkSeamLength(int seamLength) {
    if (seamLength != picture.height()) throw new IllegalArgumentException("seam length not equal picture size");
  }

  private void checkSeamConsistency(int[] seam) {
    // check that all entries in the seam are between bounds
    for (int i = 0; i < seam.length; i++)
      if (seam[i] < 0 || seam[i] >= picture.width()) {
        throw new IllegalArgumentException("seam is not consistent - seam out of bounds");
      }

    for (int i = 0; i < seam.length - 1; i++) {
      if (Math.abs(seam[i] - seam[i + 1]) > 1) {
        throw new IllegalArgumentException("seam is not consistent - difference between two seams > 1");
      }
    }
  }

  private void checkPictureSize(int pictureSize) {
    if (pictureSize <= 1) throw new IllegalArgumentException("picture is too small");
  }

  private boolean isBorderPixel(int x, int y) {
    return x == 0 || x == picture.width() - 1 || y == 0 || y == picture.height() - 1;
  }

  private double computeDualGradientEnergy(int x, int y) {
    return Math.sqrt(getXGradient(x, y) + getYGradient(x, y));
  }

  private double getXGradient(int x, int y) {
    double redDiff   = picture.get(x + 1, y).getRed()    - picture.get(x - 1, y).getRed();
    double greenDiff = picture.get(x + 1, y).getGreen()  - picture.get(x - 1, y).getGreen();
    double blueDiff  = picture.get(x + 1, y).getBlue()   - picture.get(x - 1, y).getBlue();

    return  redDiff * redDiff + greenDiff * greenDiff + blueDiff * blueDiff;
  }

  private double getYGradient(int x, int y) {
    double redDiff    = picture.get(x, y + 1).getRed()    - picture.get(x, y - 1).getRed();
    double greenDiff  = picture.get(x, y + 1).getGreen()  - picture.get(x, y - 1).getGreen();
    double blueDiff   = picture.get(x, y + 1).getBlue()   - picture.get(x, y - 1).getBlue();

    return  redDiff * redDiff + greenDiff * greenDiff + blueDiff * blueDiff;
  }
}
