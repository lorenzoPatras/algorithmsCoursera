import edu.princeton.cs.algs4.Picture;
import edu.princeton.cs.algs4.StdOut;

/**
 * Created by Kevin Wayne, Robert Sedgewick
 */
public class ShowEnergy {
  public static void main(String[] args) {
    Picture picture = new Picture("HJoceanTransposed.png");
    StdOut.printf("image is %d columns by %d rows\n", picture.width(), picture.height());
    picture.show();
    SeamCarver sc = new SeamCarver(picture);

    StdOut.printf("Displaying energy calculated for each pixel.\n");
    SCUtility.showEnergy(sc);
  }
}
