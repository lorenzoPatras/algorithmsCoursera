#ifndef QUICK_UNION_H
#define QUICK_UNION_H

#include <vector>

// Improved version for UnionFind algorithm in which merging two components p and q requires only an update s.t. id[p's root] = id[q's root] 
class QuickUnion {
public:
  QuickUnion(unsigned int n);

  bool find(unsigned int p, unsigned int q);
  void union_(unsigned int p, unsigned int q);

private:
  unsigned int getRootOf(unsigned int i);

  std::vector<unsigned int> id;
};

#endif
