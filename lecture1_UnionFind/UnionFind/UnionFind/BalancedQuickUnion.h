#ifndef BALANCED_QUICK_UNION_H
#define BALANCED_QUICK_UNION_H

#include <vector>

// Improved version of QuickUnion algorithms which avoids the case of very unballanced trees.
// Requires an extra array which holds the size of the entire tree in which the current element is node.
class BalancedQuickUnion {
public:
  BalancedQuickUnion(unsigned int n);

  bool find(unsigned int p, unsigned int q);
  void union_(unsigned int p, unsigned int q);

private:
  unsigned int getRootOf(unsigned int i);

  std::vector<unsigned int> id;
  std::vector<unsigned int> sz;
};
#endif
