#ifndef QUICK_FIND_H
#define QUICK_FIND_H

#include <vector>


// Greedy implementation of UnionFind algorithm. A vector is defined to keep the id of each component.
// Two components are connected if they have the same id. Finding if two components are connected
// takes constant time while union takes linear time.
class QuickFind {
public:
  QuickFind(unsigned int n);

  bool find(unsigned int p, unsigned int q);
  void union_(unsigned int p, unsigned int q);

private:
  std::vector<unsigned int> id;
};

#endif