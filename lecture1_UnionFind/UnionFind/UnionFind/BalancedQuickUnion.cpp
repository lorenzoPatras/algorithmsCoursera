#include <exception>

#include "BalancedQuickUnion.h"


BalancedQuickUnion::BalancedQuickUnion(unsigned int n) {
  id.resize(n);
  sz.resize(n);

  for (unsigned int i = 0; i < n; i++) {
    id[i] = i;
    sz[i] = 0;
  }
}

bool BalancedQuickUnion::find(unsigned int p, unsigned int q) {
  if (p >= id.size() || q >= id.size())
    throw std::exception("Arguments are out of bounds");

  return getRootOf(p) == getRootOf(q);
}

void BalancedQuickUnion::union_(unsigned int p, unsigned int q) {
  if (p >= id.size() || q >= id.size())
    throw std::exception("Arguments are out of bounds");

  unsigned int pRoot = getRootOf(p);
  unsigned int qRoot = getRootOf(q);

  if (pRoot == qRoot) return;

  if (sz[pRoot] < sz[qRoot]) {
    id[pRoot] = qRoot;
    sz[qRoot] = sz[pRoot];
  } else {
    id[qRoot] = pRoot;
    sz[pRoot] = sz[qRoot];
  }
}

unsigned int BalancedQuickUnion::getRootOf(unsigned int i) {
  while (i != id[i]) i = id[i];

  return i;
}
