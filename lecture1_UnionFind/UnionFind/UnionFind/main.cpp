#include "QuickFind.h"
#include "QuickUnion.h"

int main () {
  QuickFind qf(10);

  qf.union_(4, 3);
  qf.union_(3, 8);
  qf.union_(6, 5);
  qf.union_(9, 4);
  qf.union_(2, 1);
  qf.union_(8, 9);
  qf.union_(5, 0);
  qf.union_(7, 2);
  qf.union_(6, 1);
  // 11188 11188

  QuickUnion qu(10);
  qu.union_(4, 3);
  qu.union_(3, 8);
  qu.union_(6, 5);
  qu.union_(9, 4);
  qu.union_(2, 1);
  qu.union_(8, 9);
  qu.union_(5, 0);
  qu.union_(7, 2);
  qu.union_(6, 1);
  // 11183 05188

  return 0;
}