#include <exception>

#include "QuickUnion.h"


QuickUnion::QuickUnion(unsigned int n) {
  id.resize(n);

  for (unsigned int i = 0; i < n; i++) {
    id[i] = i;
  }
}

bool QuickUnion::find(unsigned int p, unsigned int q) {
  if (p >= id.size() || q >= id.size())
    throw std::exception("Arguments are out of bounds");

  return getRootOf(p) == getRootOf(q);
}

void QuickUnion::union_(unsigned int p, unsigned int q) {
  if (p >= id.size() || q >= id.size())
    throw std::exception("Arguments are out of bounds");

  id[getRootOf(p)] = getRootOf(q);
}

unsigned int QuickUnion::getRootOf(unsigned int i) {
  while (i != id[i]) i = id[i];

  return i;
}

