#include <exception>

#include "QuickFind.h"


QuickFind::QuickFind(unsigned int n) {
  id.resize(n);

  for (unsigned int i = 0; i < n; i++) {
    id[i] = i;
  }
}

bool QuickFind::find(unsigned int p, unsigned int q) {
  if (p >= id.size() || q >= id.size())
    throw std::exception("Arguments are out of bounds");

  return id[p] == id[q];
}

void QuickFind::union_(unsigned int p, unsigned int q) {
  if (p >= id.size() || q >= id.size())
    throw std::exception("Arguments are out of bounds");

  unsigned int pid = id[p];
  unsigned int qid = id[q];

  // update id of all connected components
  for (auto it = id.begin(); it != id.end(); it++) {
    if (*it == pid) *it = qid;
  }
}